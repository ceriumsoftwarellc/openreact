########################################################################
# Imports
import sys
from pathlib import Path

# Import script
if __name__ == '__main__' and __package__ is None:
  # 3 assumes 3 folders deep, like core/modules/unit . Adjust as needed
  top = Path(__file__).resolve().parents[3]
  sys.path.append(str(top))
  # Set 'core' package name
  __package__ = 'core'

# Import Parent Object
from core.unit import Unit

########################################################################
# Object Class
class OneToTwoSplitter(Unit):
  pass
