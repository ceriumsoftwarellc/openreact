########################################################################
# Imports
import sys
from pathlib import Path

## Uncomment when writing modules in core/modules/init

#~ # Import script
#~ if __name__ == '__main__' and __package__ is None:
  #~ # 3 assumes 3 folders deep, like core/modules/unit . Adjust as needed
  #~ top = Path(__file__).resolve().parents[3]
  #~ sys.path.append(str(top))
  #~ # Set 'core' package name
  #~ __package__ = 'core'
#~ 
#~ # Import Parent Object
#~ from core.unit import Unit

########################################################################
# Unit Class
class Unit():
  ######################################################################
  # Constants, Settings, and Data
  ######################################################################

  # OpenReact Object
  openreact_obj = None

  # Parent Unit Object
  parent_obj = None

  # Location of core and custom plugins folder
  core_plugins_folder = 'plugins'
  custom_plugins_folder = '../custom/plugins'

  ######################################################################
  # Settings dict
  settings = {}

  ######################################################################
  # Data dicts
  #
  # Still thinking about what to do here - we need to be able to 
  # compute:
  # - Mass balance
  # - Energy Balance
  # - Reactor Conditions (might be part of energy balance)

  # Parameters - also used when calculating degrees of freedom
  params = {}
  # List of data sources
  data_sources = ['']
  # Data to be used when doing calculations
  data = {}

  ######################################################################
  # Setup and helper Functions

  def __init__(self, **kwargs):
    if 'openreact_obj' in kwargs:
      self.openreact_obj = kwargs['openreact_obj']

  # Help: Return a list of strings to print
  def help(self):
    ret = ['This is the default unit']
    ret.append('')
    ret.append('Methods:')
    ret.append('help')
    ret.append('load_data')
    ret.append('check_data')
    ret.append('check_degf')
    ret.append('select_equations')
    return ret
    

  def load_data(self, datasources = data_sources):
    pass

  # Check input data is compatible
  def check_data(self):
    pass

  # Check degrees of freedom and that there is enough data to solve 
  # equations
  def check_degf(self):
    pass

  # Unit-specific: assign solver functions given the input data
  def select_equations(self):
    pass

  ######################################################################
  # User functions

  ######################################################################
  # Core Execution
  #
  # KWArgs:
  # - exclude: list of strings - do not execute these functions

  def execute(self, **kwargs):
    # Load our equations
    self.select_equations()
    # Load the data
    self.load_data()
    self.check_data()
    self.check_degf()
    

  

  ######################################################################
  # Report Generation Functions
  
  def report_preprocess(self, **kwargs):
    pass

  def report_process(self, **kwargs):
    pass

  def report_postprocess(self, **kwargs):
    pass

