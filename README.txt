########################################################################
# OpenReact Code Structure
########################################################################

OpenReact starts with openreact.py, which looks for a reactor/unit model
, the necessary data, and the desired output, and then executes the model 
and returns a report. It executes models iteratively, and reports may 
be generated periodically.

# General map of code structure - dashes indicate folder depth

openreact.py
- wizards
-- init-wizard.py
-- help.py
- projects
-- [project folders]
--- [init file(s)]
--- data
--- reports
- core
-- [core classes]
-- plugins
--- [plugin folders]
---- [plugin py file]
---- [plugin files]
-- models
--- analytical
--- reactor
--- unit
- custom
-- plugins
--- [custom plugin folders]
---- [custom plugin py file]
---- [custom plugin files]
-- models
--- analytical
--- reactor
--- unit

# openreact.py

OpenReact.py contains a set of core classes to calls\ all other classes
in the code structure. It requires an initializing .py file to start, 
which it uses to determine which classes need to be loaded.

## Action Points

An OpenReact instance proceeds through a series of "actions," with the 
list of actions spelled out by the initializer. As it goes through each
action, it loads a series of plugins and models to complete the action.

# Wizards Folder (do not edit)

Wizards help a user to create initialization and configuration files 
as needed. Currently (2017-04) we are only looking at initialization 
files, and browsing help data, but later on we might look into data
preparation, pre-cleaning, and compatibility checks.

# Projects folders (edit)

Data, reports, and initialization files are stored as discrete projects, 
with each project folder acting as a standalone container for user work.

Projects consist of the main initialization file, a data folder with 
user input data, and a reports folder with generated reports. The init
file should contain sufficient instructions to explain which data files
to use, and how to process them into reports.

# Core (do not edit) and Custom (edit) Folder

Core and Custom are largely identical in structure, with the main 
difference being that the core folder is not meant to be edited, and 
contains a few core classes that are essential to OpenReact.

## Core/Custom - Plugins

Plugins extend the OpenReact class at each of its action points, in a 
way that usually does not involve chemical information. Plugins can be 
thought of as helper tools to prepare data, generate reports, and so on.

Plugins are structured as a folder containing a top-level file named 
after the plugin, and after this any toher needed files or folders.

## Core/Custom - Models

Models are chemical models of various sorts that are used to do 
computation. These also contain the necessary algorithms to determine 
what to do with presented data.

Models can be analytical, reactor, or unit models. Analytical models
deal in processing analytical data, or in turning reactor data into 
anticipated analytical data. Reactor models are meant to contain the 
needed logic to simulate a reactor unit, and unit models are geared 
toward non-reactive units.

########################################################################
# How to write an initialization file
########################################################################
